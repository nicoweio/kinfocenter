/*
    SPDX-License-Identifier: GPL-2.0-only OR GPL-3.0-only OR LicenseRef-KDE-Accepted-GPL
    SPDX-FileCopyrightText: 2021 Harald Sitter <sitter@kde.org>
*/

import QtQuick 2.5

import org.kde.kcm 1.4 as KCM

import org.kde.kinfocenter.private 1.0 as KInfoCenter
import org.kde.kinfocenter.vulkan.private 1.0

KInfoCenter.CommandOutputKCM {
    KCM.ConfigModule.quickHelp: i18nc("@info", "Vulkan Graphics API Information")

    output: InfoOutputContext
}
